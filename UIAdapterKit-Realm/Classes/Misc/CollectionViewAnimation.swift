//
//  CollectionViewAnimation.swift
//  UIAdapterKit
//
//  Created by Andrea Del Fante on 10/06/2019.
//

public enum CollectionViewAnimation {
    case none
    case section
    case row
}
