//
//  TableViewAnimation.swift
//  UIAdapterKit
//
//  Created by Andrea Del Fante on 10/06/2019.
//

public enum TableViewAnimation {
    case none
    case section(_ animation: UITableView.RowAnimation)
    case row(_ animation: UITableView.RowAnimation)
}
