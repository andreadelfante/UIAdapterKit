0.3.0 Release notes (2019-06-11)
=============================================================
### Enhancements
* Add support to UITableView editing actions

0.2.0 Release notes (2019-06-09)
=============================================================
### Enhancements
* Add support to Realm Adapters

0.1.0 Release notes (2019-06-06)
=============================================================
Hello World!
