# UIAdapterKit

[![CI Status](https://img.shields.io/travis/andreadelfante/UIAdapterKit.svg?style=flat)](https://travis-ci.org/andreadelfante/UIAdapterKit)
[![Version](https://img.shields.io/cocoapods/v/UIAdapterKit.svg?style=flat)](https://cocoapods.org/pods/UIAdapterKit)
[![Platform](https://img.shields.io/cocoapods/p/UIAdapterKit.svg?style=flat)](https://cocoapods.org/pods/UIAdapterKit)
[![License](https://img.shields.io/cocoapods/l/UIAdapterKit.svg?style=flat)](https://cocoapods.org/pods/UIAdapterKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
* iOS 8.0+

## Installation

UIAdapterKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UIAdapterKit'
```

## Author

andreadelfante, andreadelfante94@gmail.com

## License

UIAdapterKit is available under the MIT license. See the LICENSE file for more info.
